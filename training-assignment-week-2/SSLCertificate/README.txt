Download openssl setup from 
	- https://code.google.com/archive/p/openssl-for-windows/downloads

Copy it to C drive
set environment variable path as 
	- C:\openssl\bin

use following command to generate certificate
	- openssl req -x509 -newkey rsa:4096 -keyout key.pem -out cert.pem -days 365 -config C:\openssl\openssl.cnf
	
Start server
Check on https://localhost:3000