var amqp = require('amqplib/callback_api');
var vmsg = "IB!!!"
amqp.connect('amqp://localhost', function(error0, connection) {
    if (error0) {
        throw error0;
    }
    connection.createChannel(function(error1, channel) {
        if (error1) {
            throw error1;
        }

        var queue = 'hello';
        // var msg = 'Hello IB!';

        channel.assertQueue(queue, {
            durable: false
        });
        channel.sendToQueue(queue, Buffer.from(vmsg));

        console.log(" [x] Sent %s", vmsg);
    });
    setTimeout(function() {
        connection.close();
        process.exit(0);
    }, 500);
});